package br.com.edol;

import javax.swing.JOptionPane;

import br.com.edol.model.No;
import br.com.edol.model.Prato;

public class Jogar {
	
	Prato prato;
    boolean jogando = true;

    public Jogar() {
        prato = new Prato();
    }
    
    /*
     * Configurar par�metros inciais padr�o da lista
     * */
    private void configurar() {
        prato.setRoot(null, "Massa", true);
        prato.setRoot(prato.getRoot(), "Lasanha", true);
        prato.setRoot(prato.getRoot(), "Bolo de Chocolate", false);
    }

    public void iniciar() {
        if (prato.getRoot() == null) {
        	configurar();
        }

        int inicializar = mostrarJogo();

        if (inicializar == JOptionPane.CLOSED_OPTION) {
        	jogando = false;
        }

        while (jogando) {
        	adivinhar(prato.getRoot());
        }
    }
    
    /*
     * Aqui s�o feitas as perguntas para o jogador escolher o prato
     * */
    public void adivinhar(No no) {
        String pergunta = "O prato que voc� pensou � " + no.getValue() + "?";
        int resposta = JOptionPane.showConfirmDialog(null, pergunta, "Confirm", JOptionPane.YES_NO_OPTION);

        if (resposta == JOptionPane.OK_OPTION) {
            if (no.isLeaf()) {
            	acerto();
            } else {
            	adivinhar(no.getDireita());
            }
        } else {
            if (no.getDireita() == null) {
            	novoPrato(no);
                iniciar();
            } else {
            	adivinhar(no.getEsquerda());
            }
        }
    }

    private int mostrarJogo() {
        Object[] options = {"Ok"};
        return JOptionPane.showOptionDialog(null, "Pense em um prato que gosta" , "Jogo Gourmet", JOptionPane.PLAIN_MESSAGE, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    }

    private void acerto() {
        JOptionPane.showMessageDialog(null, "Acertei de novo!");
        iniciar();
    }
    /*
     * 
     * Caso n�o tiver o prato na lista o jogo pergunta/salva na mem�ria o prato pensado pelo jogador.
     * */
    private void novoPrato(No no) {
        String nome = JOptionPane.showInputDialog("Qual prato voc� pensou?");
        String dica = JOptionPane.showInputDialog(nome + " � _______ mas " + no.getValue() + " n�o.");

        String wrongGuess = no.getValue();
        no.setValue(dica);
        no.setEsquerda(new No(wrongGuess));
        no.setDireita(new No(nome));
    }
	
}
