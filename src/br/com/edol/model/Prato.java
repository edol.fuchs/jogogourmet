package br.com.edol.model;

public class Prato {
	private No root;

    public No getRoot() {
		return root;
	}

	public void setRoot(No no, String valor, boolean escolha) {
		this.root = inserirNo(no, valor, escolha);;
	}

    private No inserirNo(No no, String valor, boolean escolha) {
        if (no == null) {
        	no = new No(valor);
            return no;
        } else if (escolha) {
        	no.setDireita(inserirNo(no.getDireita(), valor, escolha));
        } else {
        	no.setEsquerda(inserirNo(no.getEsquerda(), valor, escolha));
        }

        return no;
    }
}
