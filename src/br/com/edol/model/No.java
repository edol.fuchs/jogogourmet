package br.com.edol.model;

public class No {
	
	private String valor;
    private No esquerda;
    private No direita;

    public No(String data) {
    	valor = data;
    }

    public boolean isLeaf() {
        return esquerda == null && direita == null;
    }

    public void setValue(String valor) {
        this.valor = valor;
    }

    public String getValue() {
        return valor;
    }

    public void setEsquerda(No esquerda) {
        this.esquerda = esquerda;
    }

    public No getEsquerda() {
        return esquerda;
    }

    public void setDireita(No direita) {
        this.direita = direita;
    }

    public No getDireita() {
        return direita;
    }
}
